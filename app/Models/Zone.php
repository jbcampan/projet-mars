<?php

namespace App\Models;

use App\Models\Minerai;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Zone extends Model
{
    use HasFactory;

    protected $fillable = ['latitude', 'longitude', 'dangerosite', 'minerai', 'date'];

    public function minerais(){

        return $this->belongsToMany(Minerai::class);
    }
}
