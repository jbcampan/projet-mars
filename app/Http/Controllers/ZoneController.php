<?php

namespace App\Http\Controllers;

use App\Models\Zone;
use App\Models\Minerai;
use Illuminate\Http\Request;

class ZoneController extends Controller
{
    public function list() {

        $zones = Zone::all();

        return view('zone', compact('zones'));
    }

    public function show($id){

        $zone = Zone::findOrFail($id);
        
        return view('zoneShow', compact('zone'));
    }  

    public function create() {

        $minerais = Minerai::all();

        return view('zoneCreate', compact('minerais'));
    }

    public function createValidate(Request $request) {

        $zone = Zone::create([
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'dangerosite' => $request->dangerosite,
            'date'=> $request->date
        ]);

        $zone->minerais()->attach($request->minerais);

        return redirect('/zone');
    }

    public function update($id) {

        $zone = Zone::findOrFail($id);
        $minerais = Minerai::all();

        return view('zoneUpdate', compact('zone','minerais'));
    }

    public function updateValidate(Request $request, $id) {

        $zone = Zone::findOrFail($id);
        $zone->update([
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'dangerosite' => $request->dangerosite,
            'date'=> $request->date
        ]);

        $zone->minerais()->attach($request->minerais);

        return redirect('/zone');
    }
    

    public function delete($id) {

        $zone = Zone::findOrFail($id);
        $zone->delete();

        return redirect('/zone');
    }


}
