<?php

namespace App\Http\Controllers;

use App\Models\Minerai;
use Illuminate\Http\Request;

class MineraiController extends Controller
{
    public function list() {

        $minerais = Minerai::all();

        return view('minerai', compact('minerais'));
    }

    public function show($id){

        $minerai = Minerai::findOrFail($id);
        
        return view('mineraiShow', compact('minerai'));
    }  

    public function create() {

        return view('mineraiCreate');
    }

    public function createValidate(Request $request) {

        Minerai::create([
            'nom' => $request->nom,
            'dangerosite'=> $request->dangerosite,
            'description' => $request->description
        ]);

        return redirect('/minerai');
    }

    public function update($id) {

        $minerai = Minerai::findOrFail($id);

        return view('mineraiUpdate', compact('minerai'));
    }

    public function updateValidate(Request $request, $id) {

        $minerai = Minerai::findOrFail($id);
        $minerai->update([
            'nom'=> $request->nom,
            'dangerosite'=> $request->dangerosite,
            'description'=> $request->description
        ]);

        return redirect('/minerai');
    }

    public function delete($id) {

        $minerai = Minerai::findOrFail($id);
        $minerai->delete();

        return redirect('/minerai');
    }
}
