@extends('layouts.app')

@section('content')

<div class="h-25"></div>
<h1 class="text-center py-4 text-white" >Répertoire des Minerais radioactifs</h1>

<div class="container">
  <div class="row">
    <div class="col-12 col-md-8 offset-md-2">
      <table class="table text-white">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Nom</th>
            <th scope="col">Niveau de dangerosité</th>
            <th scope="col">Description</th>
            <th scope="col">Modifier</th>      
          </tr>
        </thead>
        <tbody>
          @if($minerais->count() > 0)
            @foreach($minerais as $minerai)
            <tr>
              <td>{{$minerai->nom}}</td>
              <td>{{$minerai->dangerosite}}</td>
              <td>{{$minerai->description}}</td>
              <td>
                <a class="btn btn-outline-light btn-sm" href="{{route('minerai.show', ['id' => $minerai->id])}}"><i class="fa-solid fa-circle-info"></i></a>   
                <a class="btn btn-outline-secondary btn-sm" href="{{route('minerai.update', ['id' => $minerai->id])}}"><i class="fa-regular fa-pen-to-square"></i></a>
                <a class="btn btn-outline-danger btn-sm" href="{{route('minerai.delete', ['id' => $minerai->id])}}"><i class="fa-regular fa-trash-can"></i></a>
              </td>     
            </tr>
            @endforeach

          @else
            <span>Aucun minerai en base de données</span>

          @endif
        </tbody>
      </table>

      <a class="btn btn-primary ms-1 btn" href="{{route ('minerai.create')}}">Ajouter</a>

    </div>
  </div>
</div>


@endsection