@extends('layouts.app')

@section('content')

<div class="h-25"></div>
<h1 class="text-center py-4 text-white fw-light" style="letter-spacing: 1px;">Minerai</h1>

<div class="container">
    <div class="row">
        <div class="col-8 offset-2">
            <table class="table text-white">
            <thead class="thead-dark">
                <tr>
                <th scope="col">Nom</th>
                <th scope="col">Niveau de dangerosité</th>
                <th scope="col">Description</th> 
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$minerai->nom}}</td>
                    <td>{{$minerai->dangerosite}}</td>
                    <td>{{$minerai->description}}</td>   
                </tr>
            </tbody>
            </table>

            <h2 class="text-center py-4 text-white fw-light">Modifier les informations</h2>

            <section id="contact" class="bg-transparent py-5 px-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-6 offset-md-3">
                                    <form action="{{route ('minerai.updateValidate', ['id' => $minerai->id])}}" method="post">
                                    @csrf
                                        <fieldset>
                                            <legend class="text-white fw-light">Modifiez les informations</legend>

                                            <div class="mb-3 form-floating">
                                                <input type="text" name="nom" id="nom" class="form-control" value="{{$minerai->nom}}" placeholder="Nom du minerai">
                                                <label for="nom">Nom du minerai</label>                                
                                            </div>

                                            <div class="mb-3 form-floating">
                                                <select name="dangerosite" id="dangerosite" class="form-select">
                                                    <option selected>{{$minerai->dangerosite}}</option>
                                                    <option value="faible">Faible</option>
                                                    <option value="moyen">Moyen</option>
                                                    <option value="élevé">Elevé</option>
                                                </select>
                                                <label for="description">Niveau de dangerosité du minerai</label>
                                            </div>

                                            <div class="mb-3 form-floating">
                                                <input type="text" name="description" id="description" class="form-control" value="{{$minerai->description}}" placeholder="Caractéristiques du minerai">
                                                <label for="description">Caractéristiques du minerai</label>                                
                                            </div>
                                            
                                            <!-- <div class="mb-3 form-floating">
                                                <textarea name="description" id="description" class="form-control" cols="30" rows="10" value="{{$minerai->description}}" placeholder="Caractéristiques du minerai"></textarea>
                                                <label for="description">Caractéristiques du minerai</label>
                                            </div> -->

                                        </fieldset>

                                        <button type="submit" class="btn btn-primary">Modifier</button>
                                    </form>
                                </div>
                            </div>
                        </div>            
                    </section>
        </div>
    </div>
</div>


@endsection