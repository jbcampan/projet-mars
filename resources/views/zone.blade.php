@extends('layouts.app')

@section('content')

<div class="h-25"></div>
<h1 class="text-center py-4 text-white" >Répertoire des Zones radioactives</h1>

<div class="container">
  <div class="row">
    <div class="col-12 col-md-8 offset-md-2">
      <table class="table text-white">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Latitude</th>
            <th scope="col">Longitude</th>
            <th scope="col">Dangerosité</th>
            <th scope="col">Minerai</th>
            <th scope="col">Date</th>
            <th scope="col">Modifier</th>      
          </tr>
        </thead>
        <tbody>
          @if($zones->count() > 0)
            @foreach($zones as $zone)
            <tr>
              <td>{{$zone->latitude}}</td>
              <td>{{$zone->longitude}}</td>
              <td>{{$zone->dangerosite}}</td>
              <td>@foreach($zone->minerais as $minerai)
                {{ $minerai->nom }}
                @endforeach          
              </td>
              <td>{{$zone->date}}</td>
              <td>
                <a class="btn btn-outline-light btn-sm" href="{{route('zone.show', ['id' => $zone->id])}}"><i class="fa-solid fa-circle-info"></i></a>   
                <a class="btn btn-outline-secondary btn-sm" href="{{route('zone.update', ['id' => $zone->id])}}"><i class="fa-regular fa-pen-to-square"></i></a>
                <a class="btn btn-outline-danger btn-sm" href="{{route('zone.delete', ['id' => $zone->id])}}"><i class="fa-regular fa-trash-can"></i></a>
              </td>     
            </tr>
            @endforeach

          @else
            <span>Aucune zone en base de données</span>

          @endif
        </tbody>
      </table>

      <a class="btn btn-primary ms-1 btn" href="{{route ('zone.create')}}">Ajouter </a>
    </div>
  </div>
</div>



@endsection