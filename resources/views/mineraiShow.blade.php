@extends('layouts.app')

@section('content')

<div class="h-25"></div>
<h1 class="text-center py-4 text-white">Détails du minerai: {{$minerai->nom}} </h1>

<div class="container">
  <div class="row">
    <div class="col-12 col-md-8 offset-md-2">
      <table class="table text-white">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Nom</th>
            <th scope="col">Niveau de dangerosité</th>
            <th scope="col">Description</th>  
          </tr>
        </thead>
        <tbody>
            <tr>
              <td>{{$minerai->nom}}</td>
              <td>{{$minerai->dangerosite}}</td>  
              <td>{{$minerai->description}}</td>  
            </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>


@endsection