@extends('layouts.app')

@section('content')

<div class="h-25"></div>
<h1 class="text-center py-4 text-white fw-light" style="letter-spacing: 8px;">Zone</h1>

<div class="container">
    <div class="row">
        <div class="col-8 offset-2">
            <table class="table text-white">
            <thead class="thead-dark">
                <tr>
                <th scope="col">Latitude</th>
                <th scope="col">Longitude</th>
                <th scope="col">Dangerosite</th>
                <th scope="col">Minerai</th>
                <th scope="col">Date</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$zone->latitude}}</td>
                    <td>{{$zone->longitude}}</td>
                    <td>{{$zone->dangerosite}}</td>
                    <td>@foreach($zone->minerais as $minerai)
                    {{ $minerai->nom }}
                    @endforeach          
                    </td>
                    <td>{{$zone->date}}</td>

                </tr>
            </tbody>
            </table>

            <h2 class="text-center py-4 text-white fw-light" style="letter-spacing: 1px;">Modifier les informations</h2>

            <section id="contact" class=" py-5 px-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-10 offset-md-1">
                                    <form action="{{route ('zone.updateValidate', ['id' => $zone->id])}}" method="post">
                                        @csrf
                                        <fieldset>
                                            <legend class="text-white fw-light">Modifiez les informations</legend>

                                            <div class="mb-3 form-floating">
                                                <input type="text" name="latitude" id="latitude" class="form-control" value="{{$zone->latitude}}" placeholder="Latitude de la zone">
                                                <label for="latitude">Latitude de la zone</label>                                
                                            </div>

                                            <div class="mb-3 form-floating">
                                                <input type="text" name="longitude" id="longitude" class="form-control" value="{{$zone->longitude}}" placeholder="Longitude de la zone">
                                                <label for="longitude">Longitude de la zone</label>                                
                                            </div>

                                            <div class="mb-3 form-floating">
                                                <select name="dangerosite" id="dangerosite" class="form-select">
                                                    <option selected>{{$zone->dangerosite}}</option>
                                                    @for ($i = 1; $i <= 10; $i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                                <label for="description">Niveau de dangerosité de la zone</label>
                                            </div>

                                            <div class="mb-3">
                                                <p class="h6 text-white mt-4">Quels minerais se trouvent sur les lieux ?  <span class="text-muted"> (Plusieurs minerais possibles)</span></p>
                                                <div class="form-check form-check-inline">
                                                    @foreach($minerais as $minerai)
                                                    <label class="form-check-label text-white pe-4 me-3">
                                                        <input type="checkbox" name="minerais[]" value="{{$minerai->id}}" class="form-check-input"> {{$minerai->nom}}
                                                    </label>
                                                    @endforeach
                                                </div>
                                            </div>

                                            <div class="mb-3 form-floating">
                                                <input type="date" name="date" id="date" class="form-control" value="{{$zone->date}}" placeholder="Date d'enregistrement">
                                                <label for="date">Date de modification</label>                                
                                            </div>
                                            
                                        </fieldset>

                                        <button type="submit" class="btn btn-primary">Modifier</button>
                                    </form>
                                </div>
                            </div>
                        </div>            
                    </section>
        </div>
    </div>
</div>


@endsection