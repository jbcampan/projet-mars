@extends('layouts.app')

@section('content')



<section id="contact" class="position-absolute top-50 start-50 translate-middle my-4 px-4 w-100">
    <h1 class="text-center py-4 text-white fw-light" style="letter-spacing: 8px;">Zone</h1>

    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                <form action="{{route ('zone.createValidate')}}" method="post">
                @csrf
                    <fieldset>
                        <legend class="text-white fw-light">Rentrez une nouvelle zone</legend>

                        <div class="mb-3 form-floating">
                            <input type="text" name="latitude" id="latitude" class="form-control" placeholder="Latitude de la zone">
                            <label for="latitude">Latitude de la zone</label>                                
                        </div>

                        <div class="mb-3 form-floating">
                            <input type="text" name="longitude" id="longitude" class="form-control" placeholder="Longitude de la zone">
                            <label for="longitude">Longitude de la zone</label>                                
                        </div>

                        <div class="mb-3 form-floating">
                            <select name="dangerosite" id="dangerosite" class="form-select">
                                <option selected>--- Choisissez le niveau de dangerosité de la zone</option>
                                @for ($i = 1; $i <= 10; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                            <label for="description">Niveau de dangerosité de la zone</label>
                        </div>

                        <div class="mb-3">
                            <p class="h6 text-white mt-4">Quels minerais se trouvent sur les lieux ?  <span class="text-muted"> (Plusieurs minerais possibles)</span></p>
                            <div class="form-check form-check-inline">
                                @foreach($minerais as $minerai)
                                <label class="form-check-label text-white pe-4 me-3">
                                    <input type="checkbox" name="minerais[]" value="{{$minerai->id}}" class="form-check-input"> {{$minerai->nom}}
                                </label>
                                @endforeach
                            </div>
                        </div>

                        <div class="mb-3 form-floating">
                            <input type="hidden" name="date" id="date" class="form-control" value="{{date('Y-m-d H:i:s')}}">
                        </div>
                        
                    </fieldset>

                    <button type="submit" class="btn btn-primary">Créer</button>
                </form>
            </div>
        </div>
    </div>          
</section>

<script>
    navigator.geolocation.getCurrentPosition(function(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        document.getElementById("latitude").value = latitude;
        document.getElementById("longitude").value = longitude;
    });
</script>



@endsection

