<!-- <header class="navbar navbar-expand-md px-3">
    <a class="navbar-brand" href="#">
        <img class="" alt="">
    </a>
    <button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbar-content">
        <span class="navbar-toggler-icon"></span>
    </button>
    <nav class="collapse navbar-collapse" id="navbar-content">
        <ul class="navbar-nav mx-auto pb-4">
            <li class="nav-item pe-5 text-center"><a class="nav-link" href="{{ route('accueil')}}"></a>Accueil</li>
            <li class="nav-item pe-5 text-center"><a class="nav-link" href="{{ route('zone')}}"></a>Zones</li>
            <li class="nav-item pe-5 text-center"><a class="nav-link" href="{{ route('minerai')}}"></a>Minerais</li>
        </ul>            
    </nav>
</header> -->


<header class="navbar navbar-expand p-3 bg-transparent z-3 " style="position: absolute; top: 8%; left: 50%; transform: translate(-50%, -50%);">
    <a class="navbar-brand mx-0" href="#">
        <img class="" alt="">
    </a>
    <button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbar-content">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse d-flex justify-content-center" id="navbar-content">
        <nav>
            <ul class="navbar-nav mx-auto pb-4">
                <li class="nav-item px-3"><a class="nav-link  text-white" href="{{ route('accueil')}}">Accueil</a></li>

                <li class="nav-item dropdown px-3"><a class="nav-link dropdown-toggle text-white" data-bs-toggle="dropdown" href="#">Zones</a>
                    <ul class="dropdown-menu bg-black bg-opacity-50">
                        <li><a href="{{route ('zone.create')}}" class="dropdown-item text-white nav-hover">Signaler une zone radioactive</a></li>
                        <li><a href="{{ route('zone')}}" class="dropdown-item text-white nav-hover">Répertoire des zones radioactives</a></li>
                    </ul>
                </li>

                <li class="nav-item dropdown px-3"><a class="nav-link dropdown-toggle text-white" data-bs-toggle="dropdown" href="#">Minerais</a>
                    <ul class="dropdown-menu bg-black bg-opacity-50">
                        <li><a href="{{route ('minerai.create')}}" class="dropdown-item text-white nav-hover">Ajouter un nouveau minerai radioactif</a></li>
                        <li><a href="{{ route('minerai')}}" class="dropdown-item text-white nav-hover">Répertoire des Minerais radioactifs</a></li>
                    </ul>
                </li>
            </ul>            
        </nav>
    </div>
</header>