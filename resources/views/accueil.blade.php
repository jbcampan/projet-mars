@extends('layouts.app')

@section('content')

<!-- <div id="accueil" class="position-absolute vw-100 vh-100 z-0">
    <p>bonjour</p>
</div> -->

<div class="container-fluid bg-image dune" style="background-image: url('/img/mars.jpg'); height: 100vh; background-size: cover">
    <div class="bg-noir">
        <div class="position-absolute top-50 start-50 translate-middle text-center text-white w-100">
            <h2 class="py-3 fw-normal fs-4" style="letter-spacing: 10px">Safe Space</h2>
            <h1 class="pt-3 pb-5 mx-auto w-50 fw-normal" style="letter-spacing: 5px">Déplacez-vous sereinement sur la planète Mars</h1>
            <h3 class="py-3 fw-light fs-6" style="letter-spacing: 5px">Restez informés et signalez des zones radioactives pour un espace plus safe</h3>
        </div>        
    </div>

</div>

@endsection