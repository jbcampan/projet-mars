@extends('layouts.app')

@section('content')



<section id="contact" class="position-absolute top-50 start-50 translate-middle py-5 px-4 py-5 px-4 w-100">
<h1 class="text-center py-4 text-white fw-light" style="letter-spacing: 8px;">Minerai</h1>    
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                        <form action="{{route ('minerai.createValidate')}}" method="post">
                        @csrf
                            <fieldset>
                                <legend class="text-white fw-light">Rentrez un nouveau minerai</legend>

                                <div class="mb-3 form-floating">
                                    <input type="text" name="nom" id="nom" class="form-control" placeholder="Nom du minerai">
                                    <label for="nom">Nom du minerai</label>                                
                                </div>

                                <div class="mb-3 form-floating">
                                    <select name="dangerosite" id="dangerosite" class="form-select">
                                        <option selected>--- Choisissez le niveau de dangerosité du minerai</option>
                                        <option value="faible">Faible</option>
                                        <option value="moyen">Moyen</option>
                                        <option value="élevé">Elevé</option>
                                    </select>
                                    <label for="description">Niveau de dangerosité du minerai</label>
                                </div>

                                <!-- <div class="mb-3 form-floating">
                                    <input type="text" name="description" id="description" class="form-control" placeholder="Caractéristiques du minerai">
                                    <label for="description">Caractéristiques du minerai</label>                                
                                </div> -->

                                <div class="mb-3 form-floating">
                                    <textarea name="description" id="description" class="form-control" cols="30" rows="10" placeholder="Caractéristiques du minerai"></textarea>
                                    <label for="description">Caractéristiques du minerai</label>
                                </div>
                                
                            </fieldset>

                            <button type="submit" class="btn btn-primary">Créer</button>
                        </form>
                    </div>
                </div>
            </div>            
        </section>

@endsection