@extends('layouts.app')

@section('content')

<div class="h-25"></div>
<h1 class="text-center text-white">Détails de la zone aux coordonnées: {{$zone->latitude}} /{{$zone->longitude}}</h1>


<div class="container">
    <div class="row">
        <div class="col-12 col-md-8 offset-md-2">
          <table class="table text-white">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Latitude</th>
                <th scope="col">Longitude</th>
                <th scope="col">Dangerosité</th>
                <th scope="col">Minerai</th>
                <th scope="col">Date</th>
              </tr>
            </thead>
            <tbody>
                <tr>
                  <td>{{$zone->latitude}}</td>
                  <td>{{$zone->longitude}}</td>
                  <td>{{$zone->dangerosite}}</td>
                  <td>@foreach($zone->minerais as $minerai)
                    {{ $minerai->nom }}
                    @endforeach          
                  </td>
                  <td>{{$zone->date}}</td> 
                </tr>
            </tbody>
          </table>

          <div class="d-flex justify-content-center">
            <div id="map"></div>         
          </div>

        </div>
    </div>
</div>

<style>
  #map{
    height: 400px; 
    width: 400px; 
    border-radius: 50%; 
    margin: 24px;

    box-shadow: 0px 0px 50px 7px rgba(243,165,126,0.5);
  }

  @media screen and (max-width: 576px){
    #map{
      height: 300px;
      width: 300px;
    }
  }
    
</style>

<script>

var map = L.map('map').setView([{{$zone->latitude}}, {{$zone->longitude}}], 13);
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map)

var circle = L.circle([{{$zone->latitude}}, {{$zone->longitude}}], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 1000
}).addTo(map);

</script> 
    
@endsection