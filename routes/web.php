<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ZoneController;
use App\Http\Controllers\AccueilController;
use App\Http\Controllers\MineraiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// //===================== LIST

Route::get('/', [AccueilController::class, 'nav'])->name('accueil');

Route::get('/zone', [ZoneController::class, 'list'])->name('zone');

Route::get('/minerai', [MineraiController::class, 'list'])->name('minerai');



// //===================== CREATE

Route::get('/zoneCreate', [ZoneController::class, 'create'])->name('zone.create');
Route::post('/zoneCreate', [ZoneController::class, 'createValidate'])->name('zone.createValidate');


Route::get('/mineraiCreate', [MineraiController::class, 'create'])->name('minerai.create');
Route::post('/mineraiCreate', [MineraiController::class, 'createValidate'])->name('minerai.createValidate');



// //===================== UPDATE

Route::get('/zoneUpdate{id}', [ZoneController::class, 'update'])->name('zone.update');
Route::post('/zoneUpdate{id}', [ZoneController::class, 'updateValidate'])->name('zone.updateValidate');

Route::get('/mineraiUpdate{id}', [MineraiController::class, 'update'])->name('minerai.update');
Route::post('/mineraiUpdate{id}', [MineraiController::class, 'updateValidate'])->name('minerai.updateValidate');



// //===================== DELETE

Route::get('/zoneDelete{id}', [ZoneController::class, 'delete'])->name('zone.delete');

Route::get('/mineraiDelete{id}', [MineraiController::class, 'delete'])->name('minerai.delete');



// //===================== SHOW

Route::get('/zone{id}', [ZoneController::class, 'show'])->name('zone.show');

Route::get('/minerai{id}', [MineraiController::class, 'show'])->name('minerai.show');