<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('minerais', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('dangerosite');
            $table->string('description');
            $table->timestamps();
        });
        Illuminate\Support\Facades\DB::table('minerais')->insert(
            array(
                array(
                    'nom' => 'Klingon',
                    'dangerosite' => 'Faible',
                    'description' => 'Un peu relou, altère les combinaisons, traverse les matériaux et provoque des démangeaisons cutanées, On a observé une résistance chez les geeks'
                ),
                array(
                    'nom' => 'Chomdû',
                    'dangerosite' => 'Moyen',
                    'description' => 'Le Chomdû est un minerai qui provoque des états dépressifs. Bizzarement, les personnes âgées ne sont plus affectées...',
                ),
                array(
                    'nom' => 'Perl',
                    'dangerosite' => 'Elevé',
                    'description' => 'Le Perl est le minerai à éviter à tout prix ! Hystérie, dépression, folie, voir mort subite sont les effets fréquemment observés.',
                )                  
            )
        );
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('minerais');
    }
};
